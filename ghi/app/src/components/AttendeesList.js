function AttendeesList(props) {
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Name</th>
                <th>Conference</th>
              </tr>
            </thead>
            <tbody>
              {props.attendees.map((attendee) => {
                return (
                  <tr key={attendee.href}>
                    <td>{attendee.name}</td>
                    <td>{attendee.conference}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default AttendeesList;
