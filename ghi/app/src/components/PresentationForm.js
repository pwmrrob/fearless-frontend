import React, { useEffect, useState } from 'react';

function PresentationForm() {
  const [conferences, setConferences] = useState([]);
  const [presenterName, setPresenterName] = useState('');
  const [presenterEmail, setPresenterEmail] = useState('');
  const [companyName, setCompanyName] = useState('');
  const [title, setTitle] = useState('');
  const [synopsis, setSynopsis] = useState('');
  const [conference, setConference] = useState('');

  const handleConferenceChange = (event) => {
    const value = event.target.value;
    setConference(value);
  };

  const handlePresenterNameChange = (event) => {
    const value = event.target.value;
    setPresenterName(value);
  };

  const handlePresenterEmailChange = (event) => {
    const value = event.target.value;
    setPresenterEmail(value);
  };

  const handleCompanyNameChange = (event) => {
    const value = event.target.value;
    setCompanyName(value);
  };

  const handleTitleChange = (event) => {
    const value = event.target.value;
    setTitle(value);
  };

  const handleSynopsisChange = (event) => {
    const value = event.target.value;
    setSynopsis(value);
  };

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
      //console.log(data.locations);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  // form submission
  const handleSubmit = async (event) => {
    event.preventDefault();

    // create an empty JSON object
    const data = {};

    data.presenter_name = presenterName;
    data.presenter_email = presenterEmail;
    data.company_name = companyName;
    data.title = title;
    data.synopsis = synopsis;
    data.conference = conference;

    const conferenceId = data.conference;
    //console.log(data);

    const presentationUrl = `http://localhost:8000${conferenceId}presentations/`;

    //const {conference} = formData;
    //const presentationUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`;

    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(presentationUrl, fetchConfig);
    if (response.ok) {
      const newPresentation = await response.json();
      //console.log(newConference);

      setPresenterName('');
      setPresenterEmail('');
      setCompanyName('');
      setTitle('');
      setSynopsis('');
      setConference('');
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new presentation</h1>
          <form onSubmit={handleSubmit} id="create-presentation-form">
            <div className="form-floating mb-3">
              <input
                onChange={handlePresenterNameChange}
                value={presenterName}
                placeholder="Presenter name"
                required
                type="text"
                name="presenter name"
                id="presenter name"
                className="form-control"
              />
              <label htmlFor="name">Presenter name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handlePresenterEmailChange}
                value={presenterEmail}
                placeholder="Presenter email"
                required
                type="text"
                name="presenter email"
                id="presenter email"
                className="form-control"
              />
              <label htmlFor="starts">Presenter email</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleCompanyNameChange}
                value={companyName}
                placeholder="Company name"
                required
                type="text"
                name="company name"
                id="company name"
                className="form-control"
              />
              <label htmlFor="ends">Company name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleTitleChange}
                value={title}
                placeholder="Title"
                required
                type="text"
                name="title"
                id="title"
                className="form-control"
              />
              <label htmlFor="ends">Title</label>
            </div>
            <div className="mb-3">
              <label htmlFor="synopsis">Synopsis</label>
              <textarea
                onChange={handleSynopsisChange}
                value={synopsis}
                required
                name="synopsis"
                id="synopsis"
                className="form-control"
                rows="3"
              ></textarea>
            </div>
            <div className="mb-3">
              <select
                onChange={handleConferenceChange}
                value={conference}
                required
                id="location"
                name="location"
                className="form-select"
              >
                <option value="">Choose a conference</option>
                {conferences.map((conference) => {
                  return (
                    <option key={conference.href} value={conference.href}>
                      {conference.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default PresentationForm;
