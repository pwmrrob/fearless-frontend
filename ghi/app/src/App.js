import Nav from './components/Nav';
import MainPage from './components/MainPage';
import AttendeesList from './components/AttendeesList';
import LocationForm from './components/LocationForm';
import ConferenceForm from './components/ConferenceForm';
import PresentationForm from './components/PresentationForm';
import AttendConferenceForm from './components/AttendConference';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }

  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route index element={<MainPage />} />
        <Route path="locations">
          <Route path="new" element={<LocationForm />} />
        </Route>
        <Route path="conferences">
          <Route path="new" element={<ConferenceForm />} />
        </Route>
        <Route path="presentations">
          <Route path="new" element={<PresentationForm />} />
        </Route>
        <Route path="attendees">
          <Route
            index
            element={<AttendeesList attendees={props.attendees} />}
          />
          <Route path="new" element={<AttendConferenceForm />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
