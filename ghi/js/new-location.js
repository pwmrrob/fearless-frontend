window.addEventListener('DOMContentLoaded', async () => {
  // Get the states from the API and append to select option in form
  const url = 'http://localhost:8000/api/states/';
  const response = await fetch(url);

  if (response.ok) {
    const data = await response.json();
    const selectTag = document.getElementById('state');
    for (const state of data.states) {
      const option = document.createElement('option');
      option.value = state.abbreviation;
      option.innerHTML = state.name;
      selectTag.appendChild(option);
    }
  } else {
    console.error('Bad request, try again later');
  }

  // Get new location form for event listener
  const formTag = document.getElementById('create-location-form');
  formTag.addEventListener('submit', async (event) => {
    event.preventDefault();

    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));

    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
      method: 'post',
      body: json,
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const formResponse = await fetch(locationUrl, fetchConfig);
    if (formResponse.ok) {
      formTag.reset();
      const newLocation = await formResponse.json();
    }
  });
});
