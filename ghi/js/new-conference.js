window.addEventListener('DOMContentLoaded', async () => {
  // Get the locations from the API and append to select option in form
  const url = 'http://localhost:8000/api/locations/';
  const response = await fetch(url);

  if (response.ok) {
    const data = await response.json();
    const selectTag = document.getElementById('location');
    for (const location of data.locations) {
      const option = document.createElement('option');
      option.value = location.id;
      option.innerHTML = location.name;
      selectTag.appendChild(option);
    }
  } else {
    console.error('Bad request, try again later');
  }

  // Get new location form for event listener
  const formTag = document.getElementById('create-conference-form');
  formTag.addEventListener('submit', async (event) => {
    event.preventDefault();

    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: 'post',
      body: json,
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const formResponse = await fetch(conferenceUrl, fetchConfig);
    if (formResponse.ok) {
      formTag.reset();
      const newConference = await formResponse.json();
    }
  });
});
