function createCard(name, description, pictureUrl, start, end, location) {
  const startFormat = new Date(start);
  const endFormat = new Date(end);

  return `
    <div class='col'>
      <div class="card shadow p-3 mb-5 me-2">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
          ${startFormat.toLocaleDateString()} -
          ${endFormat.toLocaleDateString()}
        </div>
      </div>
    </div>
      `;
}

function error(message) {
  return `
    <div class="alert alert-danger d-flex align-items-center" role="alert">
    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
        <div> ${message} </div>
    </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      let apiError = document.querySelector('h2');
      apiError.innerHTML += error(response.code);
    } else {
      const data = await response.json();
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const location = details.conference.location.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const start = details.conference.starts;
          const end = details.conference.ends;
          const html = createCard(
            name,
            description,
            pictureUrl,
            start,
            end,
            location
          );
          const column = document.querySelector('.row');
          column.innerHTML += html;
        }
      }
    }
  } catch (e) {
    console.error('error', e);
  }
});
